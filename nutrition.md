# NUTRITION !

[[_TOC_]]

### Fruit Nutrition Modifiers

| |Fullness|Energy|Water|Index(?)|Toxicity(?)
:---|:---:|:---:|:---:|:---:|:---:
**Raw**|1|1|1|1|1
**Rotten**|0.5|0.75|1|0.25|1
**Baked**|2|0.5|0.75|0.75|1
**Boiled**|1|1|0.8|0.8|1
**Dried**|4|0.1|0.5|0.8|1
**Burned**|1|0.25|0.75|0.1|1

### Tomato
More about [Tomato](/food/tomato.md)

| |Fullness|Energy|Water|Index(?)|Toxicity(?)
:---|:---:|:---:|:---:|:---:|:---:
**Raw**|2.5|60|95|1|0
**Rotten**|2|35|47.5|1|0
**Baked**|1.75|90|71.25|1|0
**Boiled**|1.5|75|120|1|0
**Dried**|0.75|65|9.5|1|0
**Burned**|2|35|23.75|1|0

### Apple
More about [Apple](/food/apple.md)

| |Fullness|Energy|Water|Index(?)|Toxicity(?)
:---|:---:|:---:|:---:|:---:|:---:
**Raw**|2.5|65|80|1|0
**Rotten**|2|35|40|1|0
**Baked**|1.75|100|60|1|0
**Boiled**|1.5|85|100|1|0
**Dried**|0.75|50|8|1|0
**Burned**|2|37.5|20|1|0

### Plum
More about [Plum](/food/plum.md)

| |Fullness|Energy|Water|Index(?)|Toxicity(?)
:---|:---:|:---:|:---:|:---:|:---:
**Raw**|2.5|65|80|1|0
**Rotten**|2|35|40|1|0
**Baked**|1.75|100|60|1|0
**Boiled**|1.5|85|100|1|0
**Dried**|0.75|75|8|1|0
**Burned**|2|35|20|1|0

### Pear
More about [Pear](/food/pear.md)

| |Fullness|Energy|Water|Index(?)|Toxicity(?)
:---|:---:|:---:|:---:|:---:|:---:
**Raw**|2.5|50|80|1|0
**Rotten**|2|35|40|1|0
**Baked**|1.75|87.5|60|1|0
**Boiled**|1.5|62.5|100|1|0
**Dried**|0.75|50|8|1|0
**Burned**|2|35|20|1|0

### Banana
More about [Banana](/food/banana.md)
*Not cookable*

| |Fullness|Energy|Water|Index(?)|Toxicity(?)
:---|:---:|:---:|:---:|:---:|:---:
**Raw**|1|89|128|95|1
**Rotten**|1|22.25|65|13|1

### Orange
More about [Orange](/food/orange.md)
*Not cookable*

| |Fullness|Energy|Water|Index(?)|Toxicity(?)
:---|:---:|:---:|:---:|:---:|:---:
**Raw**|1|47|131|117|1
**Rotten**|1|12|48|12|1

### Pepper
More about [Pepper](/food/pepper.md)
*Green BallPepper*

| |Fullness|Energy|Water|Index(?)|Toxicity(?)
:---|:---:|:---:|:---:|:---:|:---:
**Raw**|2.5|50|95|1|0
**Rotten**|2|35|47.5|1|0
**Baked**|1.75|75|71.25|1|0
**Boiled**|1.5|65|120|1|0
**Dried**|0.75|60|9.5|1|0
**Burned**|2|35|23.75|1|0

### Zucchini

| |Fullness|Energy|Water|Index(?)|Toxicity(?)
:---|:---:|:---:|:---:|:---:|:---:
**Raw**|2.5|50|65|1|0
**Rotten**|2|35|47.5|1|0
**Baked**|1.75|75|100|1|0
**Boiled**|1.5|65|120|1|0
**Dried**|0.75|55|9.5|1|0
**Burned**|2|35|23.75|1|0

### Pumpkin
*Not cookable*

| |Fullness|Energy|Water|Index(?)|Toxicity(?)
:---|:---:|:---:|:---:|:---:|:---:
**Raw**|2.5|20|95|1|0
**Rotten**|2|15|47.5|1|0

### Sliced Pumpkin

| |Fullness|Energy|Water|Index(?)|Toxicity(?)
:---|:---:|:---:|:---:|:---:|:---:
**Raw**|2.5|50|95|1|0
**Rotten**|2|35|47.5|1|0
**Baked**|1.75|80|71.25|1|0
**Boiled**|1.5|70|120|1|0
**Dried**|0.75|60|9.5|1|0
**Burned**|2|35|23.75|1|0

### Potato

| |Fullness|Energy|Water|Index(?)|Toxicity(?)
:---|:---:|:---:|:---:|:---:|:---:
**Raw**|2.5|20|95|1|0
**Rotten**|2|15|47.5|1|0
**Baked**|1.75|80|71.25|1|0
**Boiled**|1.5|70|95|1|0
**Dried**|0.75|20|9.5|1|0
**Burned**|2|15|23.75|1|0

### Kiwi
*Not cookable*

| |Fullness|Energy|Water|Index(?)|Toxicity(?)
:---|:---:|:---:|:---:|:---:|:---:
**Raw**|1|42|59|56|1
**Rotten**|1|15|20|10|1

### Sambucus Berry

| |Fullness|Energy|Water|Index(?)|Toxicity(?)
:---|:---:|:---:|:---:|:---:|:---:
**Raw**|2.5|50|80|1|0
**Rotten**|2|37.5|40|1|0
**Baked**|1.75|90|40|1|0
**Boiled**|1.5|90|80|1|0
**Dried**|0.75|90|8|1|0
**Burned**|2|37.5|20|1|0

### Canina Berry

| |Fullness|Energy|Water|Index(?)|Toxicity(?)
:---|:---:|:---:|:---:|:---:|:---:
**Raw**|2.5|50|80|1|0
**Rotten**|2|37.5|40|1|0
**Baked**|1.75|90|40|1|0
**Boiled**|1.5|90|80|1|0
**Dried**|0.75|90|8|1|0
**Burned**|2|37.5|20|1|0

### Cannabis

| |Fullness|Energy|Water|Index(?)|Toxicity(?)
:---|:---:|:---:|:---:|:---:|:---:
**Raw**|1|284|293|30|1
**Rotten**|1|100|293|10|1
**Baked**|1|69|172|70|1
**Boiled**|1|69|172|70|1
**Dried**|1|69|172|70|1
**Burned**|1|20|40|10|1

## Steaks

### Human

| |Fullness|Energy|Water|Index(?)|Toxicity(?)|Decease(?)
:---|:---:|:---:|:---:|:---:|:---:|:---:
**Raw**|4.5|130|70|1|0|4
**Rotten**|4|97.5|35|1|0|"4+16"
**Baked**|3.75|227.5|52.5|1|0
**Boiled**|3.5|162.5|70|1|0
**Dried**|1.8|130|7|1|0
**Burned**|4|97.5|17.5|1|0|16

### Goat

| |Fullness|Energy|Water|Index(?)|Toxicity(?)|Decease(?)
:---|:---:|:---:|:---:|:---:|:---:|:---:
**Raw**|4.5|160|70|1|0|4
**Rotten**|4|120|35|1|0|"4+16"
**Baked**|3.5|280|52.5|1|0
**Boiled**|3|200|70|1|0
**Dried**|1.5|160|7|1|0
**Burned**|4|120|17.5|1|0|16

### Mouflon

| |Fullness|Energy|Water|Index(?)|Toxicity(?)|Decease(?)
:---|:---:|:---:|:---:|:---:|:---:|:---:
**Raw**|4.5|544|334|236|1|4
**Rotten**|4|250|222|35|1|"4+16"
**Baked**|3.5|537|222|129|1
**Boiled**|3|537|222|129|1
**Dried**|1.5|390|130|180|1
**Burned**|4|250|90|50|1|16

### Boar

| |Fullness|Energy|Water|Index(?)|Toxicity(?)|Decease(?)
:---|:---:|:---:|:---:|:---:|:---:|:---:
**Raw**|4.5|130|70|1|0|4
**Rotten**|4|97.5|35|1|0|"4+16"
**Baked**|3.5|227.5|52.5|1|0
**Boiled**|3|162.5|70|1|0
**Dried**|1.5|130|7|1|0
**Burned**|4|97.5|17.5|1|0|16

### Pig

| |Fullness|Energy|Water|Index(?)|Toxicity(?)|Decease(?)
:---|:---:|:---:|:---:|:---:|:---:|:---:
**Raw**|4.5|130|70|1|0|4
**Rotten**|4|97.5|35|1|0|"4+16"
**Baked**|3.5|227.5|52.5|1|0
**Boiled**|3|162.5|70|1|0
**Dried**|1.5|130|7|1|0
**Burned**|4|97.5|17.5|1|0|16

### Dear

### Wolf

### Bear

### Cow

### Sheep

### Fox

### Chicken Breast

### Rabbit Leg

### Carp Filet

### Mackerel Fillet

### Lard
*Fat?*

### Carp

### Sardines

### Mackerel

### Worm
